# -*- coding: utf-8 -*-
__author__ = 'xead'
from sklearn.externals import joblib


class SentimentClassifier(object):
    def __init__(self):
        self.model = joblib.load("./model.pkl")
        self.classes_dict = {0: u"негативный", 1: u"позитивный", -1: u"ошибка"}

    @staticmethod
    def get_probability_words(probability):
        if probability < 0.55:
            return u"нейтральный или возможно"
        if probability < 0.7:
            return u"возможно"
        if probability > 0.95:
            return u"точно"
        else:
            return ""

    def predict_text(self, text):
        try:
            return self.model.predict([text])[0],self.model.predict_proba([text])[0].max()
        except:
            print("tprediction error")
            return -1, 0.8

    def predict_list(self, list_of_texts):
        try:
            arr=[]
            for text in list_of_texts:
                p=self.model.predict([text])[0]
                pp=self.model.predict_proba([text])[0].max()
                arr.append([p,pp])
            return arr
        except:
            print('lprediction error')
            return None

    def get_prediction_message(self, text):
        class_pl=[]
        pred_probl=[]
        out_l=[]
        if(isinstance(text,list)):
            prediction=self.predict_list(text)
            
            for p in prediction:
                class_pl.append(p[0])
                pred_probl.append(p[1])
                out_l.append(self.get_probability_words(p[1]) + " " + self.classes_dict[p[0]])
            output=out_l
        else:
            prediction = self.predict_text(text)
            class_prediction = prediction[0]
            prediction_probability = prediction[1]
            output=self.get_probability_words(prediction_probability) + " " + self.classes_dict[class_prediction]
        return output