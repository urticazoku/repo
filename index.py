from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from sentiment_classifier import SentimentClassifier
from codecs import open

app = Flask(__name__)
bootstrap = Bootstrap(app)
classifier = SentimentClassifier()

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/post.html')
def post():
	return render_template('post.html')

@app.route('/demo.html',methods=["POST","GET"])
def demo(text="",prediction_message=""):
	if request.method == "POST":
		text = request.form["text"]
		texts = text.split("~;~")
		logfile = open("ydf_demo_logs.txt", "a", "utf-8")
		if(len(texts)>1):
			prediction_message = classifier.get_prediction_message(texts)
		else:
			prediction_message = classifier.get_prediction_message(text)
		logfile.close()
	return render_template('demo.html', text=text, prediction_message=prediction_message)

if __name__ == '__main__':
	app.run(debug=True)
#test